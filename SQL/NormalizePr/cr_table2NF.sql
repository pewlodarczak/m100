CREATE TABLE customers (
    id                            SERIAL PRIMARY KEY,
    name                          VARCHAR(255),
    industry                      VARCHAR(255),
    contact_person_id             INT,
    phone_number                  VARCHAR(12),
    address                       VARCHAR(255),
    city                          VARCHAR(255),
    zip                           VARCHAR(5)
);

ALTER TABLE customers
    DROP COLUMN contact_person,
    DROP COLUMN contact_person_role,
    DROP COLUMN phone_number;


CREATE TABLE contact_persons (
    id              SERIAL PRIMARY KEY,
    name            VARCHAR(300),
    role            VARCHAR(300),
    phone_number    VARCHAR(15)
);

--- OTHER SOLUTION
CREATE TABLE project_feedbacks (
    id                  SERIAL PRIMARY KEY,
    project_id          INT, 
    customer_id         INT,
    project_feedback    TEXT
);

CREATE TABLE projects (
    id                  INT(6) AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(300),
    start_date          DATE,
    end_date            DATE
);
---