CREATE TABLE customers (
    id                            INT PRIMARY KEY,
    name                          VARCHAR(255),
    industry                      VARCHAR(255),
    contact_person_id             INT,
    contact_person                VARCHAR(300),
    contact_person_role           VARCHAR(300),
    phone_number                  VARCHAR(12),
    address                       VARCHAR(255),
    city                          VARCHAR(255),
    zip                           VARCHAR(5)
);

ALTER TABLE customers ADD COLUMN id INT PRIMARY KEY;
ALTER TABLE customers RENAME COLUMN contact_person_and_role TO contact_person;
ALTER TABLE customers ALTER COLUMN contact_person TYPE VARCHAR(300);
ALTER TABLE customers ADD COLUMN contact_person_role VARCHAR(300);
ALTER TABLE customers
    DROP COLUMN project1_id,
    DROP COLUMN project1_feedback,
    DROP COLUMN project2_id,
    DROP COLUMN project2_feedback;


CREATE TABLE project_feedbacks (
    id                  INT PRIMARY KEY,
    project_id          INT, 
    customer_id         INT,
    project_feedback    TEXT
);


--- OTHER SOLUTION
CREATE TABLE projects (
    id                  INT PRIMARY KEY,
    name                VARCHAR(300),
    start_date          DATE,
    end_date            DATE
);
---