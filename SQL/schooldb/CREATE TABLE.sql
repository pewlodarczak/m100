create database school1

CREATE TABLE public.students
(
    student_id integer NOT NULL,
    first_name character varying,
    last_name character varying,
    course character varying,
    email character varying
)

DROP database school1;
DROP DATABASE school1 WITH (FORCE);

select * from students;

INSERT INTO students (student_id, first_name, last_name, course, email) VALUES ('11', 'Dario', 'Fauci', 'DB', '');
INSERT INTO students VALUES ('12', 'Susanne', 'Maurer', 'OOP', '');

SELECT DISTINCT * FROM t_oil;
SELECT count(*) FROM t_oil;

SELECT column1, column2, ... FROM table_name1, table_name2;