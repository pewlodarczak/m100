CREATE TABLE category(
    cat_id      SERIAL PRIMARY KEY,
    category    VARCHAR(50)
);

CREATE TABLE make(
    make_id     SERIAL PRIMARY KEY,
    make        VARCHAR(50)
);

CREATE TABLE bidder(
    bidder_id     SERIAL PRIMARY KEY,
    first_name    VARCHAR(50),
    last_name     VARCHAR(50),
    bidder_rating REAL
);

INSERT INTO category(category)
SELECT DISTINCT category FROM fbay;

INSERT INTO make(make)
SELECT DISTINCT make FROM fbay;

INSERT INTO bidder(first_name, last_name, bidder_rating)
SELECT split_part(highest_bidder, ' ', 1), split_part(highest_bidder, ' ', 2), bidder_rating FROM fbay;

ALTER TABLE fbay ADD COLUMN fk_bidder_id INT CONSTRAINT fk_bidder_id
	  REFERENCES bidder(bidder_id);

ALTER TABLE fbay ADD COLUMN fk_cat_id INT CONSTRAINT fk_cat_id
	  REFERENCES category(cat_id);

ALTER TABLE fbay ADD COLUMN fk_make_id INT CONSTRAINT fk_make_id
	  REFERENCES make(make_id);

ALTER TABLE fbay ADD COLUMN product_id SERIAL PRIMARY KEY;

UPDATE fbay SET fk_bidder_id = (SELECT bidder_id FROM bidder WHERE first_name LIKE (SELECT split_part(highest_bidder, ' ', 1) FROM fbay WHERE product_id = 1)) WHERE product_id = 1;
UPDATE fbay SET fk_bidder_id = (SELECT bidder_id FROM bidder WHERE first_name LIKE (SELECT split_part(highest_bidder, ' ', 1) FROM fbay WHERE product_id = 2)) WHERE product_id = 2;
UPDATE fbay SET fk_bidder_id = (SELECT bidder_id FROM bidder WHERE first_name LIKE (SELECT split_part(highest_bidder, ' ', 1) FROM fbay WHERE product_id = 3)) WHERE product_id = 3;
UPDATE fbay SET fk_bidder_id = (SELECT bidder_id FROM bidder WHERE first_name LIKE (SELECT split_part(highest_bidder, ' ', 1) FROM fbay WHERE product_id = 4)) WHERE product_id = 4;

UPDATE fbay SET fk_cat_id = (SELECT cat_id FROM category WHERE category = (SELECT category FROM fbay WHERE product_id = 1)) WHERE product_id = 1;
UPDATE fbay SET fk_cat_id = (SELECT cat_id FROM category WHERE category = (SELECT category FROM fbay WHERE product_id = 2)) WHERE product_id = 2;
UPDATE fbay SET fk_cat_id = (SELECT cat_id FROM category WHERE category = (SELECT category FROM fbay WHERE product_id = 3)) WHERE product_id = 3;
UPDATE fbay SET fk_cat_id = (SELECT cat_id FROM category WHERE category = (SELECT category FROM fbay WHERE product_id = 4)) WHERE product_id = 4;

UPDATE fbay SET fk_make_id = (SELECT make_id FROM make WHERE make = (SELECT make FROM fbay WHERE product_id = 1)) WHERE product_id = 1;
UPDATE fbay SET fk_make_id = (SELECT make_id FROM make WHERE make = (SELECT make FROM fbay WHERE product_id = 2)) WHERE product_id = 2;
UPDATE fbay SET fk_make_id = (SELECT make_id FROM make WHERE make = (SELECT make FROM fbay WHERE product_id = 3)) WHERE product_id = 3;
UPDATE fbay SET fk_make_id = (SELECT make_id FROM make WHERE make = (SELECT make FROM fbay WHERE product_id = 4)) WHERE product_id = 4;

ALTER TABLE fbay DROP COLUMN highest_bidder, DROP COLUMN category, DROP COLUMN make, DROP COLUMN bidder_rating;

ALTER TABLE fbay RENAME TO products;

SELECT product, category, make.make, first_name FROM products 
INNER JOIN category ON category.cat_id = products.fk_cat_id
INNER JOIN make ON make.make_id = products.fk_make_id
INNER JOIN bidder ON bidder.bidder_id = products.fk_bidder_id;
