-- psql
chcp 1252
psql -U postgres

CREATE DATABASE fbay WITH encoding = 'utf8';

SHOW server_encoding;
--SET client_encoding TO 'UTF8';
SET client_encoding = 'WIN1252';
SELECT 'äöü';

CREATE TABLE fbay (
    product         VARCHAR(50),
    description     VARCHAR(200),
    num_bet         INT,
    high_bet        REAL,
    auction_end     DATE,
    highest_bidder  VARCHAR(50),
    category        VARCHAR(50),
    state           VARCHAR(50),
    make            VARCHAR(20),
    bidder_rating   REAL
);

INSERT INTO fbay VALUES(
    'iPhone X',	'Handy',	6,	500,	'30.11.2022',	'Nepomuk Hummel',	'Elektronik',	'Gebraucht', 'Apple',	4.5),
    ('Systemkamera C57',	'Kamera',	5,	350,	'12.12.2022',	'Berta Clerici',	'Elektronik',	'Neu',	'Canon',	4),
    ('Esstisch',	'Esstisch für 8 Personen',	3,	100,	'13.12.2022',	'Anastasia Spirituides',	'Möbel',	'Gebraucht',	'Splendid',	4.7),
    ('iPhone Y',	'Ein Handy', 	8,	210,	'30.11.2022',	'Linus Lanz',	'Elektronik',	'Gebraucht',	'Apple',	3.5);

TRUNCATE TABLE fbay;