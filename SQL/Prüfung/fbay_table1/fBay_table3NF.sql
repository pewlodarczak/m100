CREATE TABLE bids(
    bid_id          SERIAL PRIMARY KEY,
    high_bid        REAL,
    fk_bidder_id    INT
);

INSERT INTO bids(high_bid, fk_bidder_id) SELECT high_bet, fk_bidder_id FROM products;

ALTER TABLE products DROP column high_bet;

SELECT product, category, make.make, first_name, high_bet FROM products 
INNER JOIN category ON category.cat_id = products.fk_cat_id
INNER JOIN make ON make.make_id = products.fk_make_id
INNER JOIN bidder ON bidder.bidder_id = products.fk_bidder_id
INNER JOIN bids ON bids.fk_bidder_id = products.fk_bidder_id;
