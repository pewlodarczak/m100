CREATE DATABASE grafana;

CREATE TABLE crypto
(
    crypto_id       SERIAL PRIMARY KEY,
    open            REAL,
    high            REAL,
    low             REAL,
    close           REAL,
    volume          REAL,
    marketCap       DECIMAL,
    timestamp       TIMESTAMP,
    crypto_name     VARCHAR(200),
    date            DATE
);

SELECT crypto_name  FROM crypto;

SELECT SUM(open - (SELECT open FROM crypto WHERE crypto_name = 'Ethereum' AND date = '2015-08-11')) FROM crypto WHERE crypto_name = 'Bitcoin' AND date = '2015-08-11';
SELECT open FROM crypto WHERE crypto_name = 'Ethereum' AND date = '2015-08-11';

SELECT (open + (SELECT open FROM crypto WHERE crypto_name = 'Ethereum' AND date = '2015-08-11')) FROM crypto WHERE crypto_name = 'Bitcoin' AND date = '2015-08-11';

SELECT crypto.crypto_name, date, open FROM crypto WHERE crypto_name = 'Bitcoin'
UNION
SELECT crypto.crypto_name, date, open FROM crypto WHERE crypto_name = 'Ethereum';

SELECT (open + (SELECT open FROM crypto WHERE crypto_name = 'Ethereum')) FROM crypto WHERE crypto_name = 'Bitcoin';

SELECT sum(open) AS value, crypto_name as metric
FROM crypto WHERE crypto_name = 'Bitcoin' OR crypto_name = 'Ethereum' OR crypto_name = 'Litecoin'
GROUP BY crypto_name;

SELECT open FROM crypto WHERE crypto_name = 'Bitcoin' OR crypto_name = 'Ethereum' GROUP BY open;

SELECT open FROM crypto WHERE crypto_name = 'Ethereum';
SELECT open FROM crypto WHERE crypto_name = 'Bitcoin';

SELECT SUM(open) FROM crypto WHERE crypto_name = 'Bitcoin' OR crypto_name = 'Ethereum' GROUP BY timestamp;

SELECT EXTRACT(minutes from timestamp) AS time, SUM(open) FROM crypto WHERE crypto_name = 'Bitcoin' OR crypto_name = 'Ethereum' GROUP BY date;

SELECT EXTRACT(hours from '30 minutes'::interval);

SELECT to_char( timestamp, 'YYYY-MON-DD HH:MM:SS') as re_format from crypto;
