CREATE DATABASE grafana;


CREATE TABLE age_distribution
(
fk_country_id  INT,
age_range INT,
CONSTRAINT fk_country
FOREIGN KEY(fk_country_id) 
	  REFERENCES countries(country_id),
percentage REAL
);

INSERT INTO age_distribution values (1, '0-14', 15.34),
									(1, '15-24', 10.39),
									(1, '25-54', 42.05),
									(1,	'55-64', 13.48),
									(1, '65', 18.73);
									
INSERT INTO age_distribution values (2, '0-14', 13.45),
									(2, '15-24', 9.61),
									(2, '25-54', 40.86),
									(2,	'55-64', 14),
									(2, '65', 22.08);

SELECT * FROM age_distribution
INNER JOIN countries ON age_distribution.fk_country_id = countries.country_id;

SELECT * FROM age_distribution ORDER BY fk_country_id, age_range;

DROP TABLE age_distribution;

CREATE TABLE age_distribution
(
fk_country_id  INT,
age_range VARCHAR(10),
CONSTRAINT fk_country
FOREIGN KEY(fk_country_id) 
	  REFERENCES countries(country_id),
percentage REAL
);

INSERT INTO age_distribution values (1, '0-14', 15.34),
									(1, '15-24', 10.39),
									(1, '25-54', 42.05),
									(1,	'55-64', 13.48),
									(1, '65', 18.73),
                                    (2, '0-14', 13.45),
									(2, '15-24', 9.61),
									(2, '25-54', 40.86),
									(2,	'55-64', 14),
									(2, '65', 22.08);

SELECT country, percentage, age_range FROM age_distribution
INNER JOIN countries ON age_distribution.fk_country_id = countries.country_id
WHERE country = '$country';

SELECT country, percentage, age_range FROM age_distribution
INNER JOIN countries ON age_distribution.fk_country_id = countries.country_id
WHERE country = '$country' ORDER BY age_range DESC;

CREATE TABLE insurance
(
    age         INT,
    sex         VARCHAR(10),
    bmi         REAL,
    children    INT,
    smoker      VARCHAR(5),
    region      VARCHAR(20),
    charges     REAL
);


-- Stats
SELECT now() as time,
    count(region) AS value,
    region as metric
FROM insurance
GROUP BY region;

-- Histogram
SELECT age AS Age FROM insurance;
