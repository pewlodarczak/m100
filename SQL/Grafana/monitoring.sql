/*CREATE TABLE cpu_monitoring (time_stamp timestamp PRIMARY KEY, device_id INT, 
cpu_usage REAL);

ALTER TABLE cpu_monitoring DROP CONSTRAINT cpu_monitoring_pkey;
*/

SELECT * FROM generate_series(1,5);

SELECT * from generate_series(0,10,2);

SELECT * from generate_series(1,10) a, generate_series(1,2) b;

SELECT * from generate_series(
	'2021-01-01',
    '2021-01-02', INTERVAL '1 hour'
  );

SELECT 'Timescale!' as myStr, * FROM generate_series(1,5);

SELECT random()*100 as CPU, * FROM generate_series(1,5);

CREATE TABLE cpu_monitoring (time_stamp timestamp, device_id INT, 
cpu_usage REAL);

INSERT INTO cpu_monitoring(time_stamp, device_id, cpu_usage) 
SELECT time, device_id, random()*100 as cpu_usage 
FROM generate_series(
	'2021-01-01 00:00:00',
    '2021-01-01 06:00:00',
    INTERVAL '1 seconds'  
  ) as time, 
generate_series(1,4) device_id;

CREATE TABLE servers (server_id INT PRIMARY KEY, server_name VARCHAR(10));
INSERT INTO servers VALUES(1,'server1'),
                          (2,'server2'),
                          (3,'server3'),
                          (4,'server4');

SELECT time_stamp, server_name, cpu_usage FROM cpu_monitoring INNER JOIN
servers ON device_id=server_id WHERE server_id=1;


SELECT ts, (10 + 10 * random()) * rownum as value FROM generate_series
       ( '2020-01-01'::date
       , '2021-12-31'::date
       , INTERVAL '1 day') WITH ORDINALITY AS t(ts, rownum);


CREATE TABLE webtraffic (time_stamp timestamp, device_id INT, 
web_traffic REAL);

INSERT INTO webtraffic(time_stamp, device_id, web_traffic) 
SELECT time, device_id, random()*100 as web_traffic 
FROM generate_series(
	'2021-01-01 00:00:00',
    '2021-01-01 06:00:00',
    INTERVAL '1 seconds'
  ) as time, 
generate_series(1,4) device_id;

SELECT time_stamp AS time, server_name, web_traffic FROM webtraffic INNER JOIN
servers ON device_id=server_id WHERE server_name='$server';


SELECT  time_stamp,
 cos(rownum-1) as value
FROM generate_series('2021-01-01','2021-01-30',INTERVAL '1 day') WITH ORDINALITY AS t(ts, rownum);


CREATE TABLE wave (time_stamp timestamp, value REAL);

INSERT INTO wave(time_stamp, value)
SELECT  ts, 
cos(rownum) as value
FROM generate_series('2021-01-01','2021-03-31',INTERVAL '1 day') WITH ORDINALITY AS t(ts, rownum);

SELECT time_stamp AS time, value FROM wave;

CREATE TABLE cycle (time_stamp timestamp, value REAL);

INSERT INTO cycle (time_stamp, value)
SELECT  ts, cos(rownum * 6.28/30) as value
FROM generate_series('2021-01-01','2021-03-31',INTERVAL '1 day') WITH ORDINALITY AS t(ts, rownum);

SELECT time_stamp AS time, value FROM cycle;
