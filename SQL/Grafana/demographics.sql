create database grafana;

\c grafana

CREATE TABLE t_oil
(
region      text,
country     text,
year        int,
production  int,
consumption int
);

# Population

CREATE TABLE demography
(
country     text,
religion    text,
percentage  real
);

-- CH
-- Roman Catholic 34.4%, Protestant 22.5%, other Christian 5.7%, Muslim 5.4%, other 1.5%, none 29.4%, unspecified 1.1% (2020 est.)
-- Italy
-- Christian 80.8% (overwhelmingly Roman Catholic with very small groups of Jehovah's Witnesses and Protestants), Muslim 4.9%, unaffiliated 13.4%, other 0.9% (2020 est.)


CREATE TABLE religions 
(
religion_id		INTEGER PRIMARY KEY,
religion		text
);

COPY religions FROM 'C:/Program Files/PostgreSQL/14/data/religions.txt';

create table countries
(
country_id  INTEGER PRIMARY KEY,
country text
)

insert into countries values ('1', 'Switzerland');
insert into countries values ('2', 'Italy');

create table rel_countries
(
country_id  INT,
religion_id INT,
CONSTRAINT fk_country
FOREIGN KEY(country_id) 
	  REFERENCES countries(country_id),
CONSTRAINT fk_religion
FOREIGN KEY(religion_id) 
	  REFERENCES religion(religion_id),
percentage real
);

insert into rel_countries values (1, '1', '34.4');
insert into rel_countries values (1, '2', '22.5');
insert into rel_countries values (1, '3', '5.7');
insert into rel_countries values (1, '4', '5.4');
insert into rel_countries values (1, '5', '1.5');
insert into rel_countries values (1, '6', '29.4');
insert into rel_countries values (2, '1', '80.8');
insert into rel_countries values (2, '4', '4.9');
insert into rel_countries values (2, '6', '13.4');

DELETE FROM rel_countries WHERE country_id = 2;

select country, religion from countries, religions where country_id = 1;

select percentage, religion from rel_countries inner join religions on rel_countries.religion_id = religions.religion_id;

select country, percentage, religion from rel_countries 
inner join religions on rel_countries.religion_id = religions.religion_id 
inner join countries on rel_countries.country_id = countries.country_id where rel_countries.country_id = 1;

select country, percentage, religion from rel_countries 
inner join religions on rel_countries.religion_id = religions.religion_id 
inner join countries on rel_countries.country_id = countries.country_id where rel_countries.country_id = (SELECT country_id from countries where country = 'Italy');

select country, percentage, religion from rel_countries 
inner join religions on rel_countries.religion_id = religions.religion_id 
inner join countries on rel_countries.country_id = countries.country_id where countries.country = 'Italy';
