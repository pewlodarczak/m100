CREATE DATABASE soccer;

CREATE TABLE results
(
    edition     REAL,
    round       VARCHAR(50),
    date        DATE,
    home_club   VARCHAR(50),
    away_club   VARCHAR(50),
    home_score  INT,
    away_score  INT
);


SELECT date AS time, home_club, away_club, home_score, away_score FROM results;

SELECT home_club, away_club, home_score, away_score FROM results ORDER BY home_club;

SELECT home_club, away_club, home_score, away_score FROM results WHERE home_club = 'Alianza Lima';

SELECT date AS time, home_score, away_score FROM results WHERE home_club = 'Alianza Lima';

SELECT DISTINCT home_club FROM results ORDER BY home_club;

SELECT SUM(home_score), SUM(away_score) FROM results WHERE home_club = 'Alianza Lima';


SELECT DISTINCT away_club FROM results ORDER BY away_club;

SELECT date AS time, away_club, away_score FROM results WHERE away_club = 'Flamengo';

SELECT date AS time, home_score, away_score FROM results WHERE home_club = '$home_club1';

SELECT date AS time, away_club, away_score FROM results WHERE away_club = '$away_club';

SELECT (CASE WHEN home_score > away_score then 1 else 0 end) FROM results WHERE home_club = 'Caracas';

SELECT home_score FROM results WHERE (home_score > away_score) AND home_club = 'Caracas';

SELECT count(*) FROM results WHERE (home_score > away_score) AND home_club = 'Caracas';
