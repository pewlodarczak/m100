SELECT department_no, department_name INTO TABLE departments FROM employees;

ALTER TABLE departments ADD PRIMARY KEY (department_no);

ALTER TABLE employees RENAME COLUMN department_no TO fk_department_no;

ALTER TABLE employees
ADD CONSTRAINT fk_department_no FOREIGN KEY (fk_department_no) REFERENCES departments (department_no);

