CREATE TABLE rates (
  fk_employee_no       VARCHAR(5),
  fk_project_code      VARCHAR(5),
  hourly_rate          real,
  CONSTRAINT fk_employee
      FOREIGN KEY(fk_employee_no) 
	  REFERENCES employees(employee_no),
  CONSTRAINT fk_project
      FOREIGN KEY(fk_project_code) 
	  REFERENCES projects(project_code)
);

TRUNCATE rates;
DELETE FROM rates;

INSERT INTO rates(fk_employee_no, fk_project_code, hourly_rate)
WITH
  t1 AS (
    SELECT employee_no
    FROM employees WHERE employee_no = 'E01'
  ),
  t2 AS (
    SELECT project_code 
    FROM projects WHERE project_code = 'P01'
  ),
  t3 AS (
    SELECT hourly_rate
    FROM employees WHERE employee_no = 'E01'
  )
  SELECT t1.employee_no, t2.project_code, t3.hourly_rate
  FROM t1, t2, t3;


INSERT INTO rates(fk_employee_no, fk_project_code, hourly_rate)
WITH
  t1 AS (
    SELECT employee_no
    FROM employees WHERE employee_no = 'E05'
  ),
  t2 AS (
    SELECT project_code 
    FROM projects WHERE project_code = 'P02'
  ),
  t3 AS (
    SELECT hourly_rate
    FROM employees WHERE employee_no = 'E05'
  )
  SELECT t1.employee_no, t2.project_code, t3.hourly_rate
  FROM t1, t2, t3;

-- Other solution
INSERT INTO rates SELECT employee_no, fk_project_code, hourly_rate FROM employees;
INSERT INTO rates VALUES('P07', 'E12', 200);

ALTER TABLE employees
    DROP COLUMN hourly_rate;
