ALTER TABLE projects ADD PRIMARY KEY (project_code);

CREATE TABLE employees (
  employee_no       VARCHAR(5) PRIMARY KEY,
  fk_project_code   VARCHAR(5),
  employee_name	    VARCHAR(20),
  department_no     VARCHAR(5),
  department_name   VARCHAR(20),
  hourly_rate       real,
  CONSTRAINT fk_project
      FOREIGN KEY(fk_project_code) 
	  REFERENCES projects(project_code)
);

INSERT INTO employees(employee_no, fk_project_code, employee_name, department_no, department_name, hourly_rate)
SELECT employee_no, project_code, employee_name, department_no, department_name, hourly_rate FROM projects;

ALTER TABLE projects
    DROP COLUMN employee_no,
    DROP COLUMN employee_name,
    DROP COLUMN department_no,
    DROP COLUMN department_name,
    DROP COLUMN hourly_rate;

