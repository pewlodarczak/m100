CREATE DATABASE projects;

CREATE TABLE projects (
  project_code    VARCHAR(5),
  project_name	  VARCHAR(20),
  project_manager	VARCHAR(20),
  project_budget	real,
  employee_no     VARCHAR(5),
  employee_name	  VARCHAR(20),
  department_no   VARCHAR(5),
  department_name	VARCHAR(20),
  hourly_rate     real
);

INSERT INTO projects values ('P01',	'Ecommerce Solution',	'Mr. Smith',	120000,	'E01',	'Marthaler',	'D03',	'Software Engineering',	200),
                            ('P02',	'Reservation System',	'Mrs. Ellis',	250000,	'E05',	'Wegelin',	'D05',	'Database',	210);

