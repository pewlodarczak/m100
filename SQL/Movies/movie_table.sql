CREATE DATABASE movies;

CREATE TABLE movies(
    full_name	    VARCHAR(20),
    address	        VARCHAR(100),
    movies_rented	TEXT,
    salutation	    VARCHAR(5),
    age	            INT,
    rental          VARCHAR(20),
    filiale         VARCHAR(20)
);

INSERT INTO movies VALUES('Phil Jones',	'Nowherestreet 77 89000 Nowhereopolis',	'Pirates of the carribean
James Bond
Harry Potter',	'Mr',	35,	'Zug',	'Bahnhofstrasse'),
('Emma Parkins',	'Manhattanstreret 45 67033 Someplaceopolis',	'Clash of the titans
Marshal', 'Mrs',	45,	'Luzern',	'Hechtplatz'),
('Magnus Furrer',	'Theaterstrasse 14 Zug',	'Terminator
Rambo
Schneewittchen',	'Mr',	33,	'Zug',	'Neumatt');

