CREATE TABLE city (
  city        VARCHAR(50),
  zip				  INT,
  city_id			SERIAL PRIMARY KEY
);

INSERT INTO city(city, zip)
SELECT DISTINCT city, zip FROM address;

ALTER TABLE address ADD COLUMN fk_city_id INT CONSTRAINT fk_city_id
	  REFERENCES city(city_id);

UPDATE address SET fk_city_id = (SELECT city_id FROM city WHERE city = 'Zug')
WHERE city = 'Zug';

UPDATE address SET fk_city_id = (SELECT city_id FROM city WHERE city = 'Nowhereopolis')
WHERE city = 'Nowhereopolis';

UPDATE address SET fk_city_id = (SELECT city_id FROM city WHERE city = 'Someplaceopolis')
WHERE city = 'Someplaceopolis';

SELECT street, city.city  FROM address INNER JOIN city ON city.city_id=address.fk_city_id 
 ORDER BY city;

ALTER TABLE address DROP COLUMN city, DROP COLUMN zip;
