ALTER TABLE movies RENAME TO customers;
ALTER TABLE customers ADD COLUMN customer_id SERIAL PRIMARY KEY;

CREATE TABLE address (
  addr_id           SERIAL PRIMARY KEY,
  street            VARCHAR(50),
  street_no 	      INT,
  city              VARCHAR(50),
  zip               INT
);

CREATE TABLE movies (
  movie_id          SERIAL PRIMARY KEY,
  name              VARCHAR(50)
);

CREATE TABLE rental (
  rental_id         SERIAL PRIMARY KEY,
  rental_name       VARCHAR(50),
  city              VARCHAR(50)
);

CREATE TABLE movies_rented (
    fk_customer_id  INT,
    fk_movie_id     INT,
    fk_rental_id    INT,
    CONSTRAINT fk_customer
      FOREIGN KEY(fk_customer_id) 
	  REFERENCES customers(customer_id),
    CONSTRAINT fk_movie
      FOREIGN KEY(fk_movie_id) 
	  REFERENCES movies(movie_id),
        CONSTRAINT fk_rental
      FOREIGN KEY(fk_rental_id) 
	  REFERENCES rental(rental_id)
);

ALTER TABLE customers ADD COLUMN last_name VARCHAR(20);

UPDATE customers SET full_name = split_part(full_name, ' ', 1),
                     last_name = split_part(full_name, ' ', 2);

ALTER TABLE customers RENAME COLUMN full_name TO first_name;

INSERT INTO rental(rental_name, city)
SELECT DISTINCT filiale, rental FROM customers;

INSERT INTO movies(name)
SELECT split_part(movies_rented, E'\n', 1) FROM customers;
INSERT INTO movies(name)
SELECT split_part(movies_rented, E'\n', 2) FROM customers;
INSERT INTO movies(name)
SELECT split_part(movies_rented, E'\n', 3) FROM customers;

DELETE FROM movies WHERE movie_id=8;

INSERT INTO movies_rented(fk_customer_id, fk_movie_id, fk_rental_id)
WITH
  t1 AS (
    SELECT customer_id FROM customers WHERE movies_rented LIKE '%' || (SELECT name FROM movies WHERE movie_id = 3) || '%'
  ),
  t2 AS (
    SELECT rental_id FROM rental WHERE rental_name = (SELECT filiale FROM customers WHERE movies_rented LIKE '%' || (SELECT name FROM movies WHERE movie_id = 3) || '%')
  )
  SELECT t1.customer_id, 3, t2.rental_id
  FROM t1, t2;


-- SELECT rental_id FROM rental WHERE rental_name = (SELECT filiale FROM customers WHERE movies_rented LIKE '%' || (SELECT name FROM movies WHERE movie_id = 13) || '%');

INSERT INTO address(street, street_no, zip, city)
SELECT split_part(address, ' ', 1), CAST(split_part(address, ' ', 2) AS INT), 
CAST(split_part(address, ' ', 3) AS INT), split_part(address, ' ', 4)
FROM customers WHERE customer_id = 1 OR customer_id = 2;

INSERT INTO address(street, street_no, city)
SELECT split_part(address, ' ', 1), CAST(split_part(address, ' ', 2) AS INT), 
split_part(address, ' ', 3)
FROM customers WHERE customer_id = 3;

ALTER TABLE customers ADD COLUMN fk_address_id INT CONSTRAINT fk_address_id
	  REFERENCES address(addr_id);

UPDATE customers SET fk_address_id = (SELECT addr_id FROM address WHERE street LIKE (SELECT split_part(address, ' ', 1) FROM customers WHERE customer_id = 1))
WHERE customer_id = 1;
UPDATE customers SET fk_address_id = (SELECT addr_id FROM address WHERE street LIKE (SELECT split_part(address, ' ', 1) FROM customers WHERE customer_id = 2))
WHERE customer_id = 2;
UPDATE customers SET fk_address_id = (SELECT addr_id FROM address WHERE street LIKE (SELECT split_part(address, ' ', 1) FROM customers WHERE customer_id = 3))
WHERE customer_id = 3;

SELECT first_name, street FROM customers INNER JOIN address ON customers.fk_address_id=address.addr_id;

ALTER TABLE customers DROP COLUMN address, DROP COLUMN movies_rented, DROP COLUMN rental, DROP COLUMN filiale;

-- DELETE FROM movies_rented WHERE fk_customer_id=4 AND fk_movie_id=13;

SELECT first_name, name, rental_name FROM movies_rented INNER JOIN customers ON customers.customer_id=movies_rented.fk_customer_id 
INNER JOIN movies ON movies.movie_id=movies_rented.fk_movie_id
INNER JOIN rental ON rental.rental_id=movies_rented.fk_rental_id ORDER BY first_name;

