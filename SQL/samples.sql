CREATE TABLE sample (
    id                            SERIAL PRIMARY KEY,
    name                          VARCHAR(255),
    industry                      VARCHAR(255)
)

INSERT INTO sample VALUES (DEFAULT,'name1', 'industry1'),
(DEFAULT,'name2', 'industry2');


ALTER TABLE sample ALTER COLUMN name TYPE varchar(100);
