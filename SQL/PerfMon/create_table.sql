--CREATE TABLE monitoring (time_stamp timestamp, server varchar(20), cpu_load real, cpu_perf real, ram_load real, ram_usage real);
INSERT INTO monitoring VALUES ('1/2/2022 04:05', 'srv1', 20.7, 2.4, 30.6, 9);
INSERT INTO monitoring VALUES ('1/2/2022 04:06', 'srv1', 25.7, 2.6, 47.6, 11);

CREATE TABLE monitoring (time_stamp timestamp PRIMARY KEY, server_id INT, 
cpu_load real, fk_cpu_metric INT, cpu_perf real, ram_load real, ram_usage real,
CONSTRAINT fk_server
FOREIGN KEY(server_id)
	  REFERENCES servers(server_id),
CONSTRAINT fk_metric
FOREIGN KEY(fk_cpu_metric)
	  REFERENCES metrics(metric_id)
						);

	  
