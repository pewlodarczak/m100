﻿using System;
using PGDev;

class MainProgram
{
    public static void Main(String[] args)
    {
        PgAccess pga = new PgAccess();
        //pga.CheckVersion();
        // pga.CreateAndPopulateTable();
        // pga.TruncateTable("cars");
        // pga.ImportValues("./Data/cars.csv", "cars");
        //pga.ImportValuesPrepare("./Data/cars.csv", "cars");
        //pga.ReadData("cars");
        //pga.ReadDataWithHeader("cars");

        //pga.CreateImageTable();
        //DbDialogue dbDialogue = new();
        //dbDialogue.ShowDialogue();
        //BlogPgAccess blogPgAccess = new();
        //blogPgAccess.CreateTable();
        BlogDialogue blogDialogue = new();
        blogDialogue.ShowDialogue();
    }
}