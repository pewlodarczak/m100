namespace PGDev;

class DbDialogue
{
    public void ShowDialogue()
    {
        string input = "";
        int number;
        Console.Clear();
        Console.WriteLine();
        do {
            Console.WriteLine("\t###########################");
            Console.WriteLine("\t1\tUpload file");
            Console.WriteLine("\t2\tShow files in DB");
            Console.WriteLine("\t3\tDownload image");
            Console.WriteLine("\t4\tDelete image");
            Console.WriteLine("\t###########################");
            Console.WriteLine();
            Console.WriteLine();
            input = Console.ReadLine();
            if(input == "")
                return;
            number = int.Parse(input);

            switch(number)
            {
                case 1:
                    Console.Clear();
                    UploadFile();
                    break;
                case 2:
                    Console.Clear();
                    new PgAccess().ListImages();
                    break;
                case 3:
                    DownloadImages();
                    break;
                case 4:
                    DeleteImage();
                    break;
            }
        } while (input != "");
        return;
    }

    private string UploadFile()
    {
        // TODO verify if file already in DB
        string[] files = Directory.GetFiles("C:/Program Files/PostgreSQL/15/img");
        foreach (string f in files)
        {
            Console.WriteLine($"\t{f}");
        }
        string file = "";
        Console.WriteLine("\n\tWhich file should be oploaded?");
        file = Console.ReadLine();
        if(file.Equals(""))
            return null;
        foreach (string f in files)
        {
            if(f.Contains(file)) {
                Console.WriteLine($"\tUploading {f}\n");
                string[] subs = f.Split('\\');
                //Console.WriteLine("File name: " + subs[subs.Length-1]);
                new PgAccess().InsertImage(subs[subs.Length-1], f);
                return f;
            }
        }
        Console.WriteLine($"{file} not found");
        return null;
    }

    private void DownloadImages()
    {
        Console.WriteLine("\n\tWhich file should be downloaded?\n");
        var pga = new PgAccess();
        pga.ListImageNames();
        string image = Console.ReadLine();
        pga.GetImage(image);
    }
    
    private void DeleteImage()
    {
        // TODO file not found exception
        Console.WriteLine("\n\tWhich file should be deleted?\n");
        var pga = new PgAccess();
        pga.ListImageNames();
        string image = Console.ReadLine();
        pga.DeleteImage(image);
    }
}
