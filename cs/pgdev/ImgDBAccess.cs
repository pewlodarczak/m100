namespace PGDev;

using Npgsql;

class PgAccess
{
    string cs = "Host=localhost;Username=postgres;Port=5433;Password=admin;Database=csdev";
    public void CheckVersion()
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();
        var sql = "SELECT version()";
        using var cmd = new NpgsqlCommand(sql, con);
        var version = cmd.ExecuteScalar().ToString();
        Console.WriteLine($"PostgreSQL version: {version}");
        con.Close();
    }

    public void CreateAndPopulateTable()
    {
        using var con = new NpgsqlConnection(cs); // Call Dispose when Onject goes out of scope (IDispose)
        con.Open();
        using var cmd = new NpgsqlCommand();
        cmd.Connection = con;

        cmd.CommandText = "DROP TABLE IF EXISTS cars";
        cmd.ExecuteNonQuery();

        cmd.CommandText = @"CREATE TABLE cars(id SERIAL PRIMARY KEY,
        name VARCHAR(255), price INT)";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "INSERT INTO cars(name, price) VALUES('Tesla',60000)";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "INSERT INTO cars(name, price) VALUES('Maseratti',300000)";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "INSERT INTO cars(name, price) VALUES('Lamborghini',200000)";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "INSERT INTO cars(name, price) VALUES('Volvo',29000)";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "INSERT INTO cars(name, price) VALUES('Bentley',350000)";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "INSERT INTO cars(name, price) VALUES('Lotus',120000)";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "INSERT INTO cars(name, price) VALUES('Hummer',41400)";
        cmd.ExecuteNonQuery();

        cmd.CommandText = "INSERT INTO cars(name, price) VALUES('Volkswagen',21600)";
        cmd.ExecuteNonQuery();

        Console.WriteLine("Table cars created");
        con.Close();
    }

    public void TruncateTable(string TableName)
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();
        var sql = "TRUNCATE TABLE " + TableName;
        using var cmd = new NpgsqlCommand(sql, con);
        cmd.ExecuteNonQuery();
        Console.WriteLine($"Table {TableName} truncated");
        con.Close();
    }

    public void ImportValues(string Path, string TableName)
    {
        using var con = new NpgsqlConnection(cs); // Call Dispose when Onject goes out of scope (IDispose)
        con.Open();
        using var cmd = new NpgsqlCommand();
        cmd.Connection = con;

        List<string> listA = new List<string>();
        List<string> listB = new List<string>();

        using(var reader = new StreamReader(Path))
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                listA.Add(values[0]);
                listB.Add(values[1]);
                cmd.CommandText = $"INSERT INTO cars(name, price) VALUES('{values[0]}',{values[1]})";
                cmd.ExecuteNonQuery();
            }
        }

        foreach(string s in listA) 
        {
            Console.WriteLine(s);
        }
        con.Close();
    }

    public void ImportValuesPrepare(string Path, string TableName)
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();

        var sql = "INSERT INTO cars(name, price) VALUES(@name, @price)";
        using var cmd = new NpgsqlCommand(sql, con);
        // var listA = new List<string>();
        // var listB = new List<string>();
        
        using(var reader = new StreamReader(Path))
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                Console.WriteLine($"name {values[0]} price {Int32.Parse(values[1])}");
                cmd.Parameters.AddWithValue("name", values[0]);
                cmd.Parameters.AddWithValue("price", Int32.Parse(values[1]));
                cmd.Prepare();
                cmd.ExecuteNonQuery();
            }
        }
        con.Close();
    }

    public void ReadData(string TableName)
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();

        string sql = "SELECT * FROM cars";
        using var cmd = new NpgsqlCommand(sql, con);
        using NpgsqlDataReader rdr = cmd.ExecuteReader();

        while (rdr.Read())
        {
            Console.WriteLine("{0}\t{1}\t\t{2}", rdr.GetInt32(0), rdr.GetString(1),
                    rdr.GetInt32(2));
        }
        con.Close();
    }

    public void ReadDataWithHeader(string TableName)
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();

        var sql = "SELECT * FROM cars";

        using var cmd = new NpgsqlCommand(sql, con);

        using NpgsqlDataReader rdr = cmd.ExecuteReader();
        Console.WriteLine($"{rdr.GetName(0),-4} {rdr.GetName(1),-10} {rdr.GetName(2),10}");

        while (rdr.Read())
        {
        Console.WriteLine($"{rdr.GetInt32(0),-4} {rdr.GetString(1),-10} {rdr.GetInt32(2),10}");
        }
    }

    public void CreateImageTable()
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();
        using var cmd = new NpgsqlCommand();
        cmd.Connection = con;

        cmd.CommandText = "DROP TABLE IF EXISTS images";
        cmd.ExecuteNonQuery();

        cmd.CommandText = @"CREATE TABLE images(id SERIAL PRIMARY KEY,
        name VARCHAR(255), image bytea)";
        cmd.ExecuteNonQuery();
        con.Close();
    }

    public void InsertImage(string imgName, string path)
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();
        using var cmd = new NpgsqlCommand();
        cmd.Connection = con;
        cmd.CommandText = $"INSERT INTO images(name, image) values('{imgName}', pg_read_binary_file('{path}'))";
        //Console.WriteLine("SQL: " + cmd.CommandText);
        cmd.ExecuteNonQuery();
        con.Close();
    }

    public void ListImages()
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();

        string sql = "SELECT id, name FROM images";
        using var cmd = new NpgsqlCommand(sql, con);
        using NpgsqlDataReader rdr = cmd.ExecuteReader();
        Console.WriteLine("\n\n\t---------------------------");
        while (rdr.Read())
        {
            Console.WriteLine("\t{0}\t{1}", rdr.GetInt32(0), rdr.GetString(1));
            Console.WriteLine("\t---------------------------");
        }
        Console.WriteLine("\n\n");
        con.Close();
    }

    public void GetImage(string imgName)
    {
        try {
            using var con = new NpgsqlConnection(cs);
            con.Open();
            using var command = new NpgsqlCommand($"select image from images where name = '{imgName}';", con);
            Byte[] result = (Byte[])command.ExecuteScalar();
            FileStream fs = new FileStream($"C:/Users/Peter/projects/dbdev/cs/img/{imgName}", FileMode.Create, FileAccess.Write);
            BinaryWriter bw = new BinaryWriter(new BufferedStream(fs));
            bw.Write(result);
            bw.Flush();
            fs.Close();
            bw.Close();
            con.Close();
        }
        catch(FileNotFoundException ex) {
            Console.WriteLine(ex.ToString());
        }
        catch(ArgumentNullException ex) {
            Console.WriteLine($"\nArgument {imgName} must be a valid file name!\n");
        }
    }

    public void ListImageNames()
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();

        string sql = "SELECT name FROM images";
        using var cmd = new NpgsqlCommand(sql, con);
        using NpgsqlDataReader rdr = cmd.ExecuteReader();
        Console.WriteLine("\n\n\t---------------------------");
        while (rdr.Read())
        {
            Console.WriteLine("\t{0}", rdr.GetString(0));
            Console.WriteLine("\t---------------------------");
        }
        Console.WriteLine("\n\n");
        con.Close();
    }

    public void DeleteImage(string ImageName)
    {
        using var con = new NpgsqlConnection(cs); // Call Dispose when Onject goes out of scope (IDispose)
        con.Open();
        using var cmd = new NpgsqlCommand();
        cmd.Connection = con;
        cmd.CommandText = $"DELETE FROM images WHERE name='{ImageName}';";
        cmd.ExecuteNonQuery();
        con.Close();
    }
}