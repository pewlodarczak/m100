using Npgsql;

class BlogPgAccess
{
    string cs = "Host=localhost;Username=postgres;Port=5433;Password=admin;Database=csdev";
    public void CreateTable()
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();
        using var cmd = new NpgsqlCommand();
        cmd.Connection = con;

        cmd.CommandText = "DROP TABLE IF EXISTS blogs";
        cmd.ExecuteNonQuery();

        cmd.CommandText = @"CREATE TABLE blogs(id SERIAL PRIMARY KEY,
        title VARCHAR(50), post TEXT, author VARCHAR(50), timestamp TIMESTAMP)";
        cmd.ExecuteNonQuery();
    }

    public void InsertBlog(string title, string post, string name)
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();
        using var cmd = new NpgsqlCommand();
        cmd.Connection = con;
        cmd.CommandText = $"INSERT INTO blogs(title, post, author, timestamp) VALUES('{title}', '{post}', '{name}', now())";
        cmd.ExecuteNonQuery();
        con.Close();
    }

    public void ListBlogs()
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();

        string sql = "SELECT title, author FROM blogs";
        using var cmd = new NpgsqlCommand(sql, con);
        using NpgsqlDataReader rdr = cmd.ExecuteReader();
        Console.WriteLine("\n\n\t---------------------------");
        while (rdr.Read())
        {
            Console.WriteLine("\t{0}\t{1}", rdr.GetString(0), rdr.GetString(1));
            Console.WriteLine("\t---------------------------");
        }
        Console.WriteLine("\n\n");
        con.Close();
    }

    public void Find(string term)
    {
        using var con = new NpgsqlConnection(cs);
        con.Open();

        string sql = $"SELECT title, post, author, timestamp FROM blogs WHERE to_tsvector(post) @@ to_tsquery('{term}')";

        using var cmd = new NpgsqlCommand(sql, con);
        using NpgsqlDataReader rdr = cmd.ExecuteReader();
        Console.WriteLine("\n\n\t---------------------------");
        while (rdr.Read())
        {
            Console.WriteLine("\t{0}\t{1}\t{2}\t{3}", rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetDateTime(3));
            Console.WriteLine("\t---------------------------");
        }
        Console.WriteLine("\n\n");
        con.Close();
    }
}