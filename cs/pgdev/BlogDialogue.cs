class BlogDialogue
{
    public void ShowDialogue()
    {
        string input = "";
        int number;
        Console.Clear();
        Console.WriteLine();
        do {
            Console.WriteLine("\t###########################");
            Console.WriteLine("\t1\tNew blog");
            Console.WriteLine("\t2\tShow blogs");
            Console.WriteLine("\t3\tFind in blog");
            Console.WriteLine("\t4\tDelete blog");
            Console.WriteLine("\t###########################");
            Console.WriteLine();
            Console.WriteLine();
            input = Console.ReadLine();
            if(input == "")
                return;
            number = int.Parse(input);

            switch(number)
            {
                case 1:
                    Console.Clear();
                    NewBlog();
                    break;
                case 2:
                    Console.Clear();
                    new BlogPgAccess().ListBlogs();
                    break;
                case 3:
                    Find();
                    break;
                case 4:
                    //TODO DELETE Blog post
                    break;
            }
        } while (input != "");
        return;
    }

    private void NewBlog()
    {
        Console.WriteLine("Enter your name:");
        string name = Console.ReadLine();
        Console.WriteLine("Enter title:");
        string title = Console.ReadLine();
        Console.WriteLine("Enter blog:");
        string line = "", post = "";
        do
        {
            line = Console.ReadLine();
            post += line + "\n";
        } while (line != "");
        new BlogPgAccess().InsertBlog(title, post, name);
    }

    private void Find()
    {
        Console.WriteLine("Enter search term:");
        string term = Console.ReadLine();
        new BlogPgAccess().Find(term);        
    }
}
